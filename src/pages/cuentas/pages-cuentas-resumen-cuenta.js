/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';

class pagesCuentasResumenCuenta extends PolymerElement {

    static get properties() {
        return {
            tsec: {
                type: String,
                value: window.sessionStorage.getItem("tsec")
            },
            loading:{
                type: Boolean,
                value: true
            },
            tipoCuenta:{
                type: String,
                value:''
            },
            balance:{
                type: String,
                value:''
            },
            tipoMoneda:{
                type: String,
                value:''
            },
            numeroCuenta:{
                type: String,
                value:''
            },
            accountObjectId:{
                type: String,
                value:''
            }

        };
    }

    connectedCallback() {
        super.connectedCallback();
        console.log("Verificar presencia de TSEC");
        let tsec = window.sessionStorage.getItem("tsec");
        if (!tsec) {
            window.location.href = "/inicio";
            return;
        }
    }

    irDetalle(e){
        console.log("Disparando evento: ir-detalle-cuenta");
        this._fireEvent('ir-detalle-cuenta', { accountObjectId: this.accountObjectId});
    }

    _fireEvent(eventName, payload = null) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail: payload }));
    }

    static get template() {
        return html`
        <style>
            :host {
            display: block;
            font-family: Helvetica;
            }
        </style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <div class="card mb-3">
            <div class="card-header row m-0">
                <p class="col-6 col-sm-6 text-left m-0"> [[ tipoCuenta ]] </p>
                <p class="col-6 col-sm-6 text-right m-0"> Disponible:  [[ balance ]] </p>
            </div>
            <div class="card-body">
            <p class="card-text"><h4 class=" badge-info">Tipo de moneda</h4>  [[tipoMoneda]]</p>
            <p class="card-text"><h4 class=" badge-info">Número de cuenta</h4>  [[numeroCuenta]]</p>
            <button class="btn btn-outline-primary" on-click="irDetalle">Ver detalle</button>
            </div>
        </div>
        `;
    }
}

window.customElements.define('pages-cuentas-resumen-cuenta', pagesCuentasResumenCuenta);
