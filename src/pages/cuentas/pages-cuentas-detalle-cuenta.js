/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';

class pagesCuentasDetalleCuenta extends PolymerElement {

    static get properties() {
        return {
            tsec: {
                type: String,
                value: window.sessionStorage.getItem("tsec")
            },
            loading: {
                type: Boolean,
                value: true
            },
            objectCuentaDetalle: {
                type: Object,
                value: {}
            },
            sinOperaciones: {
                type: Boolean,
                value: false
            }
        };
    }

    // Observe the name sub-property on the user object
    static get observers() {
        return [
            'objectCuentaDetalleChanged(objectCuentaDetalle)'
        ]
    }

    objectCuentaDetalleChanged(objectCuentaDetalle) {
        console.log("Se disparo un cambio en el atributo"+objectCuentaDetalle);
        this.sinOperaciones = this.objectCuentaDetalle.operations.length == 0 ? true : false;
    }

    connectedCallback() {
        console.log("Disparando ConnectCallback");
        super.connectedCallback();
        console.log("Verificar presencia de TSEC");
        let tsec = window.sessionStorage.getItem("tsec");
        if (!tsec) {
            window.location.href = "/inicio";
            return;
        }
        console.log(this.objectCuentaDetalle);
        this.sinOperaciones = this.objectCuentaDetalle.operations.length == 0 ? true : false;
    }

    disconnectedCallback() {
        console.log("Disparando disconnectCallback")
        super.disconnectedCallback();
    }

    regresarResumen(e) {
        console.log("Disparando evento: ir-resumen-cuentas");
        this._fireEvent('ir-resumen-cuentas');
    }

    _fireEvent(eventName, payload = null) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail: payload }));
    }

    static get template() {
        return html`
        <style>
            :host {
            display: block;
            font-family: Helvetica;
            }
        </style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <div class="card mb-3">
            <div class="card-header row m-0">
                <p class="col-6 col-sm-6 text-left m-0"> [[ objectCuentaDetalle.account_type ]] </p>
                <p class="col-6 col-sm-6 text-right m-0"> Disponible:  [[ objectCuentaDetalle.balance_format ]] </p>
            </div>
            <div class="card-body">
            <p class="card-text"><h4 class=" badge-info">Tipo de moneda</h4>  [[objectCuentaDetalle.currency_large_format]]</p>
            <p class="card-text"><h4 class=" badge-info">Número de cuenta</h4>  [[objectCuentaDetalle.account_number_format]]</p>
            <p class="card-text"><h4 class=" badge-info">Número de cuenta interbancaria</h4>  [[objectCuentaDetalle.cci_format]]</p>
            <p class="card-text"><h4 class=" badge-info">Fecha de apertura</h4>  [[objectCuentaDetalle.account_opening_date]]</p>
            <button class="btn btn-outline-primary" on-click="regresarResumen">Regresar al resumen</button>
            </div>
        </div>
        <h3>Ultimos movimientos</h3>
        <table class="table">
        <thead>
            <tr>
            <th scope="col">N° Operación</th>
            <th scope="col">Fecha</th>
            <th scope="col">Concepto</th>
            <th scope="col">Monto</th>
            </tr>
        </thead>
        <tbody>
            <template is="dom-repeat" items="[[ objectCuentaDetalle.operations ]]" as="operacion">
                <tr>
                    <th scope="row"> [[ operacion._id ]]</th>
                    <td>[[ operacion.transaction_date ]]</td>
                    <td>[[ operacion.operation_concept_desc ]]</td>
                    <td>[[ operacion.operation_amount ]]</td>
                </tr>
            </template>
        </tbody>
    </table>

    <div class="container-fluid text-center" hidden$="[[ !sinOperaciones ]]"> 
        <h3 class="badge-warning">No tienes ningun movimiento</h3>
    </div>

        `;
    }
}

window.customElements.define('pages-cuentas-detalle-cuenta', pagesCuentasDetalleCuenta);
