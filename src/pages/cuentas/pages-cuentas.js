/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax';
import '../../shared-styles';
import '../util/pages-util-loading.js';
import '../util/pages-util-navbar.js';
import './pages-cuentas-resumen-cuenta.js';
import './pages-cuentas-detalle-cuenta.js';

const CUENTAS_STATUS = {
    resumen: 'resumen',
    detalle: 'detalle'
};

class pagesCuentas extends PolymerElement {

    static get properties() {
        return {
            tsec: {
                type: String,
                value: window.sessionStorage.getItem("tsec")
            },
            customerId: {
                type: String,
                value: window.sessionStorage.getItem("customerId")
            },
            accountObjectId: {
                type: String,
                value: ''
            },
            costumerFullName: {
                type: String,
                value: window.sessionStorage.getItem("customerFullName")
            },
            loading: {
                type: Boolean,
                value: true
            },
            listadoCuentas: {
                type: Object,
                value: {}
            },
            detalleCuenta: {
                type: Object,
                value: {}
            },
            step: {
                type: String,
                value: CUENTAS_STATUS.resumen
            }
        };
    }

    connectedCallback() {
        super.connectedCallback();
        console.log("Verificar presencia de TSEC");
        if (!this.tsec) {
            window.location.href = "/inicio";
            return;
        }
        this._consultarResumenCuentas();
    }

    _consultarResumenCuentas(){
        this.$.doConsultarResumenCuentas.headers['tsec'] = this.tsec;
        this.$.doConsultarResumenCuentas.body = JSON.stringify({});
        this.$.doConsultarResumenCuentas.generateRequest();
    }

    _consultarDetalleCuentas(){
        //Consultando el detalle de una cuenta
        this.$.doConsultarDetalleCuenta.headers['tsec'] = this.tsec;
        this.$.doConsultarDetalleCuenta.body = JSON.stringify({});
        this.$.doConsultarDetalleCuenta.url = this._contruirUrlDetalle();
        this.$.doConsultarDetalleCuenta.generateRequest();
    }

    procesarResumenCuentasCliente(e) {
        console.log("Procesando la consulta de resumen de las cuentas");
        window.sessionStorage.setItem("tsec", e.detail.response.tsec);
        this.listadoCuentas = e.detail.response.data.accounts;
        window.sessionStorage.setItem("listadoCuentas", JSON.stringify(this.listadoCuentas));
        this.step = CUENTAS_STATUS.resumen;
        this.loading = false;
    }

    procesarDetalleCuentaCliente(e){
        console.log("Procesando la consulta del detalle de una cuenta");
        window.sessionStorage.setItem("tsec", e.detail.response.tsec);
        this.detalleCuenta = e.detail.response.data;
        this.step = CUENTAS_STATUS.detalle;
        this.loading = false;
    }

    manejarError(e) {
        window.location.href = '/inicio';
    }

    onIrDetalleCuenta(e) {
        console.log("Evento capturado: " + e);
        this.loading = true;
        this.accountObjectId = e.detail.accountObjectId;
        this._consultarDetalleCuentas();
    }

    onIrResumenCuentas(e){
        console.log("Evento capturado: " + e);
        this.loading = true;
        this._consultarResumenCuentas();
    }

    _fireEvent(eventName, payload = null) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail: payload }));
    }

    _shouldShowStepComponent(step, phase) {
        return step === phase
    }

    _contruirUrlResumen() {
        return `${MyAppGlobals.rootApiBack}/customers/${this.customerId}/accounts`;
    }

    _contruirUrlDetalle() {
        return `${MyAppGlobals.rootApiBack}/customers/${this.customerId}/accounts/${this.accountObjectId}`;
    }

    static get template() {
        return html`
        <style>
            :host {
            display: block;
            font-family: Helvetica;
            }
        </style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <pages-util-navbar
            disabled-cuentas = 'true'
            disabled-operaciones = 'false'
        ></pages-util-navbar>
        <pages-util-loading hidden$="[[ !loading ]]"></pages-util-loading>

        <div hidden$="[[ loading ]]">
            
            <div class="container-fluid row text-left m-0 p-0">
                <div class="col-8 col-sm-8 offset-2">
                <h1>CUENTAS</h1>
                <template is="dom-if" if="[[ _shouldShowStepComponent(step, 'resumen') ]]">
                    <p>Aqui encontrarás un resumen de las cuentas que posees</p>
                    <template is="dom-repeat" items="{{listadoCuentas}}" as="cuenta">
                        <pages-cuentas-resumen-cuenta
                            tipo-cuenta="[[ cuenta.account_type ]]"
                            balance="[[ cuenta.balance_format ]]"
                            tipo-moneda="[[ cuenta.currency_large_format  ]]"
                            numero-cuenta="[[ cuenta.account_number_format ]]"
                            numero-cuenta-interbancario="[[ cuenta.cci_format ]]"
                            fecha-apertura="[[ cuenta.account_opening_date ]]"
                            account-object-id="[[ cuenta._id ]]"
                            on-ir-detalle-cuenta="onIrDetalleCuenta"
                        ></pages-cuentas-resumen-cuenta>
                    </template>
                </template>
                
                <template is="dom-if" if="[[ _shouldShowStepComponent(step, 'detalle') ]]">
                    <p>Este es el detalle de la cuenta, al final aparecen los ultimos movimientos</p>
                    <pages-cuentas-detalle-cuenta
                        object-cuenta-detalle="[[ detalleCuenta ]]"
                        on-ir-resumen-cuentas="onIrResumenCuentas"
                    ></pages-cuentas-detalle-cuenta>
                </template>
                </div>
            </div>

        </div>


        <iron-ajax 
        id="doConsultarResumenCuentas"
        url="[[ _contruirUrlResumen() ]]"
        content-type="application/json"
        method="GET"
        handle-as="json"
        on-response="procesarResumenCuentasCliente"
        on-error="manejarError">

        <iron-ajax 
        id="doConsultarDetalleCuenta"
        content-type="application/json"
        method="GET"
        handle-as="json"
        on-response="procesarDetalleCuentaCliente"
        on-error="manejarError">
        `;
    }
}

window.customElements.define('pages-cuentas', pagesCuentas);
