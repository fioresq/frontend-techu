/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../../shared-styles.js';

class PagesUsuariosSharedConfirmar extends PolymerElement {

  static get properties() {
    return {
      procesoEjecutado: {
        type: String,
        value: '' 
      },
      dniOfuscado:{
          type: String,
          value: ''
      }
    };
  }

  _determinarProceso(procesoEjecutado, phase){
    return procesoEjecutado===phase;
  }

  irIniciarSesion(e) {
    window.location.href = '/inicio'
  }

  static get template() {
    return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }

      .container {
        width: 100%;
        max-width: 600px;
        margin: 0 auto;
        padding: 100px 0;
        top: 100px;
      }

      .col-md-12 {
        font-family: Helvetica;
        color: white;
      }

      .btn-primary {
        background: #0b8585 !important;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <div class="container-fluid row mx-0 mt-3">
      <h1 class="col-12 col-sm-12 text-center text-success"> ¡Eso es todo! </h1>

      <p 
        hidden$="[[ !_determinarProceso(procesoEjecutado, 'inscripcion') ]]" 
        class="col-6 col-sm-6 offset-3 text-center"> 
        Ya te lograste inscribir en la banca por internet. 
      </p>
      <p 
        hidden$="[[ !_determinarProceso(procesoEjecutado, 'modificacion') ]]" 
        class="col-6 col-sm-6 offset-3 text-center"
      > 
        Ya modificaste correctamente tu clave de la banca por internet. 
      </p>
      <p class="col-6 col-sm-6 offset-3 text-center"> Ahora podras iniciar sesión con tu nueva clave y tu DNI terminado en [[ dniOfuscado ]] </p>

      <button class="btn btn-primary col-sm-2 offset-5 " on-click="irIniciarSesion">Inicio</button>

    </div>

    `;
  }
}

window.customElements.define('pages-usuarios-shared-confirmar', PagesUsuariosSharedConfirmar);
