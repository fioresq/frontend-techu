/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../../shared-styles.js';

class PagesUsuariosSharedError extends PolymerElement {

  static get properties() {
    return {
      botonIniciarSesion: {
        type: String,
        value: '' 
      },
      mensajeError:{
          type: String,
          value: ''
      }
    };
  }

  _mostrarBoton(phase){
    return this.botonIniciarSesion===phase;
  }

  irIniciarSesion(e) {
    window.location.href = '/inicio'
  }

  static get template() {
    return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }

      .container {
        width: 100%;
        max-width: 600px;
        margin: 0 auto;
        padding: 100px 0;
        top: 100px;
      }

      .col-md-12 {
        font-family: Helvetica;
        color: white;
      }

      .btn-primary {
        background: #0b8585 !important;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <div class="container-fluid row mx-0 mt-3">
        <h1 class="col-12 col-sm-12 text-center text-success"> ¡Oops! </h1>

        <p class="col-6 col-sm-6 offset-3 text-center"> Algo ha salido mal, te recomendamos intentarlo más tarde. </p>
        <h5
            class="col-12 col-sm-12 text-center badge-danger my-1 mx-0 py-0 px-0"> 
            [[ mensajeError ]]
        </h5>

         <button hidden$="[[ !_mostrarBoton('true') ]]"  class="btn btn-primary col-sm-2 offset-5 " on-click="irIniciarSesion">Inicio</button>

    </div>

    `;
  }
}

window.customElements.define('pages-usuarios-shared-error', PagesUsuariosSharedError);
