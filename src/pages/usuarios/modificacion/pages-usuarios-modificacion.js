/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax';
import '../shared/pages-usuarios-shared-confirmar.js';
import '../../util/pages-util-loading';
import '../shared/pages-usuarios-shared-error.js';
import '../../../shared-styles.js';

const MODIFICACION_STATUS = {
  INITIALIZE: 'initialize',
  PROCCESING: 'proccesing',
  ERROR: 'error',
  DONE: 'done'
};

class PagesUsuariosModificacion extends PolymerElement {

  static get properties() {
    return {
      card_number: {
        type: String,
        value: ''
      },
      pin: {
        type: String,
        value: ''
      },
      password: {
        type: String,
        value: ''
      },
      password_repeat: {
        type: String,
        value: ''
      },
      step: {
        type: String,
        value: MODIFICACION_STATUS.INITIALIZE
      },
      dniOfuscado: {
        type: String,
        value: ''
      },
      ocultarPass: {
        type: Boolean,
        value: true
      },
      inputType: {
        type: String,
        value: 'password'
      },
      huboError: {
        type: Boolean,
        value: false
      },
      mensajeError: {
        type: String,
        value: ''
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    window.sessionStorage.removeItem("tsec");
    window.sessionStorage.removeItem("customerId");
    window.sessionStorage.removeItem("customerFullName");
  }

  _reiniciarErrorFrontal() {
    this.huboError = false;
    this.mensajeError = '';
  }

  _mostrarErrorFrontal(mensaje) {
    this.huboError = true;
    this.mensajeError = mensaje;
  }

  procesarCambioPassword(e, request) {
    let response = e.detail.response;
    console.log(response);
    if (response && response.state == 'success') {
      this.dniOfuscado = response.data.customer.personal_id;
      this.step = MODIFICACION_STATUS.DONE;
    }
  }

  manejarError(e) {
    this.mensajeError = e.detail.request.xhr.response.error.message;
    this.step = MODIFICACION_STATUS.ERROR;
  }

  modificarClaveCliente(e) {
    console.log("Iniciando proceso de modificación de contraseña del cliente");
    this._reiniciarErrorFrontal();

    console.log(this.password)
    if (this.card_number.length != 16 && !isNaN(this.card_number)) {
      //Se lanza un error por que las claves no coinciden
      console.log('El Card Number no es un número de 16 digitos');
      this._mostrarErrorFrontal('El número de tarjeta debe tener 16 digitos');
      return;
    }

    if (this.pin.length != 4 && !isNaN(this.pin)) {
      //Se lanza un error por que las claves no coinciden
      console.log('El PIN no es un número de 4 digitos');
      this._mostrarErrorFrontal('El número de PIN debe tener 4 digitos');
      return;
    }

    if (this.password != this.password_repeat) {
      //Se lanza un error por que las claves no coinciden
      console.log('Los password no coinciden.');
      this._mostrarErrorFrontal('Las dos contraseñas deben coincidir');
      return;
    }

    if (this.password.length < 5) {
      //Se lanza un error por que las claves no coinciden
      console.log('El password es menor a 5 digitos');
      this._mostrarErrorFrontal('La contraseña debe tener más de 5 caracteres');
      return;
    }

    console.log('Lanzando la petición de modificación de clave');
    let requestData = {
      "card_pan_id": this.card_number,
      "card_pin_id": this.pin,
      "password": this.password
    };

    this.$.doInscripcionCliente.body = JSON.stringify(requestData);
    this.$.doInscripcionCliente.generateRequest();

    this.step = MODIFICACION_STATUS.PROCCESING;

  }

  irIniciarSesion(e) {
    window.location.href = '/inicio'
  }

  mostrarOcultarClave() {
    this.inputType = !this.ocultarPass ? "password" : "text";
    this.ocultarPass = !this.ocultarPass;
  }

  _contruirUrl() {
    return `${MyAppGlobals.rootApiBack}/users`
  }

  _verificarPaso(step, phase) {
    return step === phase;
  }


  static get template() {
    return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <template is="dom-if" if="[[ _verificarPaso(step, 'initialize') ]]">
      <div class="container-fluid row mx-0 mt-3" >
        <h1 class="col-6 col-sm-6 offset-3 text-center">¿No recuerdas tu clave?</h1>
        <p class="col-6 col-sm-6 offset-3 text-center">¡No te preocupes! Puedes recuperarla de la misma forma como la creaste</p>
        <div class="card col-6 col-sm-6 offset-3" alt="Responsive card">
          <div class="card-body">
            <div class="form-group">
              <input type="tel" class="form-control" id="card_number" placeholder="Número de tarjeta" value="{{card_number::input}}" pattern="[0-9]{16}" required>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="pin" placeholder="Clave de cajero" value="{{pin::input}}" pattern="[0-9]{4}" required>
            </div>
            <div class="form-group">
              <input type="[[ inputType ]]" class="form-control" id="password" placeholder="Contraseña" value="{{password::input}}" required>
            </div>
            <div class="form-group">
              <input type="[[ inputType ]]" class="form-control" id="password_repeat" placeholder="Repite contraseña" value="{{password_repeat::input}}" required>
            </div>
            <div class="container-fluid row p-0 m-0">
              <button class="btn btn-primary col-sm-3" on-click="irIniciarSesion">Inicio</button>
              <button class="btn btn-primary col-sm-4 offset-1" on-click="mostrarOcultarClave">Mostrar clave</button>
              <button class="btn btn-primary col-sm-3 offset-1" on-click="modificarClaveCliente">Actualizar</button>
            </div>
            <p 
              hidden$="[[ !huboError ]]" 
              class="col-12 col-sm-12 text-center badge-danger mt-2"> 
              [[ mensajeError ]]
            </p>
          </div>
        </div>
      </div>
    </template>

    <template is="dom-if" if="[[ _verificarPaso(step, 'proccesing') ]]">
      <pages-util-loading></pages-util-loading>
    </template>

    <template is="dom-if" if="[[ _verificarPaso(step, 'done') ]]">
      <pages-usuarios-shared-confirmar 
       proceso-ejecutado= "modificacion"
        dni-ofuscado="[[ dniOfuscado ]]">
      </pages-usuarios-shared-confirmar>
    </template>

    <template is="dom-if" if="[[ _verificarPaso(step, 'error') ]]">
      <pages-usuarios-shared-error
      boton-iniciar-sesion="true"
      mensaje-error="[[ mensajeError ]]" 
      ></pages-usuarios-shared-error>
    </template>

    <iron-ajax 
    id="doInscripcionCliente"
    url="[[ _contruirUrl() ]]"
    content-type="application/json"
    method="PUT"
    handle-as="json"
    headers='{"Access-Control-Allow-Origin": "*"}'
    on-response="procesarCambioPassword"
    on-error="manejarError">

    `;
  }
}

window.customElements.define('pages-usuarios-modificacion', PagesUsuariosModificacion);
