/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax';
import '../../shared-styles.js';

class PagesInicio extends PolymerElement {

    static get properties() {
        return {
            dni: {
                type: String,
                value: ''
            },
            password: {
                type: String,
                value: ''
            },
            dniOfuscado: {
                type: String,
                value: ''
            },
            ocultarPass: {
                type: Boolean,
                value: true
            },
            inputType: {
                type: String,
                value: 'password'
            }
        };
    }

    connectedCallback() {
        super.connectedCallback();
        window.sessionStorage.removeItem("tsec");
        window.sessionStorage.removeItem("customerId");
        window.sessionStorage.removeItem("customerFullName");
    }

    _reiniciarErrorFrontal() {
        this.huboError = false;
        this.mensajeError = '';
    }

    _mostrarErrorFrontal(mensaje) {
        this.huboError = true;
        this.mensajeError = mensaje;
    }

    _determinarProceso(procesoEjecutado, phase) {
        return procesoEjecutado === phase;
    }

    _contruirUrl() {
        return `${MyAppGlobals.rootApiBack}/auth`;
    }

    irInscribirse(e) {
        window.location.href = '/inscripcion';
    }

    procesarInicioSesion(e, request) {
        let response = e.detail.response;
        if (response && response.state == 'success') {
            window.location.href = '/cuentas';
            window.sessionStorage.setItem('tsec',response.tsec);
            window.sessionStorage.setItem('customerId',response.data.customerId);
            window.sessionStorage.setItem('customerFullName',response.data.customerFullName);
        }
    }   

    manejarError(e) {
        this._mostrarErrorFrontal(e.detail.request.xhr.response.error.message);
    }

    mostrarOcultarClave() {
        this.inputType = !this.ocultarPass ? "password" : "text";
        this.ocultarPass = !this.ocultarPass;
    }

    iniciarSesion(e) {
        console.log("Iniciando proceso de inicio de sesion");
        this._reiniciarErrorFrontal();
        console.log(this.password)
        if (this.dni.length != 8 && !isNaN(this.card_number)) {
            //Se lanza un error por que las claves no coinciden
            console.log('El dni no es un número de 8 digitos');
            this._mostrarErrorFrontal('El número de tarjeta debe tener 8 digitos');
            return;
        }

        if (this.password < 5) {
            //Se lanza un error por que las claves no coinciden
            console.log('Password menor a 5 digitos');
            this._mostrarErrorFrontal('Su contraseña tiene más de 5 digitos');
            return;
        }

        console.log('Lanzando la petición de inicio de sesión');
        let requestData = {
            "personal_id": this.dni,
            "password": this.password
        };

        this.$.doIniciarSesion.body = JSON.stringify(requestData);
        this.$.doIniciarSesion.generateRequest();

    }

    static get template() {
        return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <div class="container-fluid row mx-0 mt-3">
        <h1 class="col-6 col-sm-6 offset-3 text-center badge-secondary">Ingresa a tu banca por internet</h1>
        <div class="card col-6 col-sm-6 offset-3" alt="Responsive card">
          <div class="card-body">
            <div class="form-group">
              <input type="text" class="form-control" id="dni" placeholder="DNI" value="{{dni::input}}" pattern="[0-9]{4}" required>
            </div>
            <div class="form-group">
              <input type="[[ inputType ]]" class="form-control" id="password" placeholder="Contraseña" value="{{password::input}}" required>
            </div>
            <div class="alert alert-light text-center m-0 p-0" role="alert">
                ¿Olvidaste tu contraseña? <a href="/modificacion" class="alert-link">Haz clic aquí</a>
            </div>
            <div class="container-fluid row p-0 m-0">
              <button class="btn btn-primary col-sm-3" on-click="irInscribirse">Inscribirse</button>
              <button class="btn btn-primary col-sm-4 offset-1" on-click="mostrarOcultarClave">Mostrar contraseña</button>
              <button class="btn btn-primary col-sm-3 offset-1" on-click="iniciarSesion">Ingresar</button>
            </div>
            <h5
              hidden$="[[ !huboError ]]" 
              class="col-12 col-sm-12 text-center badge-danger my-1 mx-0 py-0 px-0"> 
              [[ mensajeError ]]
            </h5>
          </div>
        </div>
      </div>


      <iron-ajax 
      id="doIniciarSesion"
      url="[[ _contruirUrl() ]]"
      content-type="application/json"
      method="POST"
      handle-as="json"
      headers='{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "*"}'
      on-response="procesarInicioSesion"
      on-error="manejarError">
    `;
    }
}

window.customElements.define('pages-inicio', PagesInicio);
