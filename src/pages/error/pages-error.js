/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../../shared-styles.js';

class PagesUsuariosInscripcion extends PolymerElement {

  irIniciarSesion(e) {
    window.location.href='/inicio'
  }


  static get template() {
    return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }

      .container {
        width: 100%;
        max-width: 600px;
        margin: 0 auto;
        padding: 100px 0;
        top: 100px;
      }

      .col-md-12 {
        font-family: Helvetica;
        color: white;
      }

      .btn-primary {
        background: #0b8585 !important;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <h1> ERROR  </h1>


    `;
  }
}

window.customElements.define('pages-usuarios-inscripcion', PagesUsuariosInscripcion);
