/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';

class pagesUtilLoading extends PolymerElement {

  static get properties() {
    return {};
  }

  static get template() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <div class="container" style="margin-top:20%">
        <div class="row justify-content-center align-items-center minh-100 h-100 w-100 p-0 m-0">
            <div class="spinner-grow text-center" role="status">
                <span class="sr-only text-center">Loading...</span>
            </div>
        </div>
    </div>
    `;
  }
}

window.customElements.define('pages-util-loading', pagesUtilLoading);
