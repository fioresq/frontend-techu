/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles';

class pagesUtilNavbar extends PolymerElement {

    static get properties() {
        return {
            customerFullName: {
                type: String,
                value: window.sessionStorage.getItem("customerFullName")
            },
            disabledCuentas: {
                type: String,
                value: ''
            },
            disabledOperaciones: {
                type: String,
                value: ''
            }
        };
    }


    cerrarSesion(e) {
        window.sessionStorage.removeItem("tsec");
        window.sessionStorage.removeItem("customerId");
        window.sessionStorage.removeItem("customerFullName");
        window.location.href = '/inicio';
    }

    _determinarDisabledCuentas(){
        return !(this.disabledCuentas == 'true');
    }

    _determinarDisabledOperaciones(){
        return !(this.disabledOperaciones == 'true');
    }

    static get template() {
        return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <div class="container-fluid row w-100 m-0 p-0" >
        <nav class="navbar navbar-light bg-light w-100">
            <div class="container-fluid w-100 m-0 p-0 row">
                <h2 class="col-sm-2 col-2 badge-secondary text-center">Banco TechU</h2>
                <h3 class="col-2 col-sm-2"> [[ customerFullName ]] </h3>
                <button class="btn btn-outline-danger col-1 col-sm-1 offset-1" on-click="cerrarSesion">Cerrar sesión</button>
            </div>
        </nav>
    </div>

    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link disabled" hidden$="[[ _determinarDisabledCuentas() ]]" href="/cuentas">Cuentas</a>
            <a class="nav-link" hidden$="[[ !_determinarDisabledCuentas() ]]" href="/cuentas">Cuentas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" hidden$="[[ _determinarDisabledOperaciones() ]]" href="/operaciones">Operaciones</a>
            <a class="nav-link" hidden$="[[ !_determinarDisabledOperaciones() ]]" href="/operaciones">Operaciones</a>
        </li>
    </ul>
    <hr class="m-0">
    `;
    }
}

window.customElements.define('pages-util-navbar', pagesUtilNavbar);
