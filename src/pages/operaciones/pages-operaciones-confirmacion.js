/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';

class pagesOperacionesConfirmacion extends PolymerElement {

    static get properties() {
        return {
            objectTransferencia: {
                type: Object,
                value: {}
            },
            habraConversion: {
                type: Boolean,
                value: ''
            }
        };
    };

    // Observe the name sub-property on the user object
    static get observers() {
        return [
            'objectTransferenciaChanged(objectTransferencia)'
        ]
    }

    objectTransferenciaChanged(objectTransferencia) {
        console.log("Se disparo un cambio en el atributo" + JSON.stringify(objectTransferencia));
        console.log(objectTransferencia);
        this.habraConversion = this.objectTransferencia.objectCuentaDestino.currency_id != this.objectTransferencia.objectCuentaOrigen.currency_id ? true : false;
        console.log(this.habraConversion);
    }

    connectedCallback() {
        this.habraConversion = this.objectTransferencia.objectCuentaDestino.currency_id != this.objectTransferencia.objectCuentaOrigen.currency_id ? true : false;
    }

    _fireEvent(eventName, payload = null) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail: payload }));
    }

    realizarTransferencia(e) {
        this._fireEvent('realizar-transferencia-bancaria');
    }

    regresarFormulario(e) {
        this._fireEvent('regresar-formulario-transferencia-bancaria');
    }

    static get template() {
        return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <div class="card mb-3">
        <div class="card-body row text-center">
            <div class="container col-sm-12 col-12 badge-info">
                <h3>Revisa tu operación</h3> 
            </div>
            <div class="container col-sm-6 col-6">
                <p class="card-text">
                    <h4 class=" badge-info">Origen</h4>
                    [[ objectTransferencia.objectCuentaOrigen.currency_id ]] [[ objectTransferencia.objectCuentaOrigen.account_id ]]
                </p>
            </div>
            <div class="container col-sm-6 col-6">
                <p class="card-text">
                    <h4 class=" badge-info">Destino</h4>
                    [[ objectTransferencia.objectCuentaDestino.currency_id ]] [[  objectTransferencia.objectCuentaDestino.account_id ]]
                </p>
            </div>
            <div class="container col-sm-6 col-6">
                <p class="card-text">
                    <h4 class=" badge-info">Monto a transferir</h4>
                    [[ objectTransferencia.objectCuentaOrigen.currency_id ]] [[  objectTransferencia.montoOperacion ]]
                </p>
            </div>
            <div class="container col-sm-6 col-6">
                <p class="card-text">
                    <h4 class=" badge-info">Beneficiario</h4>
                    [[  objectTransferencia.objectCuentaDestino.beneficiario ]]
                </p>
            </div>
            <div class="container col-sm-12 col-12" hidden$="[[ !objectTransferencia.conceptoOperacion ]]">
                <p class="card-text">
                    <h4 class=" badge-info">Concepto</h4>
                    [[  objectTransferencia.conceptoOperacion ]]
                </p>
            </div>
            <div class="container col-sm-12 col-12 mt-2" hidden$="[[ !habraConversion ]]">
                <h4 class=" badge-warning">SE APLICARA CONVERSIÓN DE MONEDA</h4>
            </div>
            <div class="container-fluid row p-0 m-0">
                <button class="btn btn-primary col-sm-4" on-click="regresarFormulario">Atras</button>
                <button class="btn btn-primary col-sm-4 offset-4" on-click="realizarTransferencia">Confirmar</button>
            </div>
        </div>
    </div>

    `;
    }
}

window.customElements.define('pages-operaciones-confirmacion', pagesOperacionesConfirmacion);
