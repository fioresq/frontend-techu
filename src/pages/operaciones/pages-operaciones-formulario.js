/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';
import '@polymer/iron-ajax';
import '../util/pages-util-loading.js';
import '../util/pages-util-navbar.js';

class pagesOperacionesFormulario extends PolymerElement {

    static get properties() {
        return {
            tsec: {
                type: String,
                value: window.sessionStorage.getItem('tsec')
            },
            huboError: {
                type: Boolean,
                value: false
            },
            mensajeError: {
                type: String,
                value: ''
            },
            loading: {
                type: Boolean,
                value: false
            },
            objectCuentaOrigen: {
                type: Object,
                value: ''
            },
            objectCuentaDestino: {
                type: Object,
                value: ''
            },
            numeroCuentaDestino: {
                type: String,
                value: ''
            },
            listadoCuentasUsuario: {
                type: Object,
                value: JSON.parse(window.sessionStorage.getItem("listadoCuentas"))
            },
            conceptoOperacion: {
                type: String,
                value: ''
            },
            montoOperacion: {
                type: String,
                value: ''
            },
            customerId: {
                type: String,
                value: window.sessionStorage.getItem('customerId')
            }
        };
    };


    connectedCallback() {
        console.log("Disparando evento connectedCallback");
        console.log(this.listadoCuentasUsuario);
        this.objectCuentaOrigen = JSON.stringify({
            _id: this.listadoCuentasUsuario[0]._id,
            account_id: this.listadoCuentasUsuario[0].account_number_format,
            currency_id: this.listadoCuentasUsuario[0].currency_id
        });
    }

    _resumenCuenta(cuenta) {
        return `${cuenta.account_number_format}  Saldo: ${cuenta.balance_format}`
    }

    _construirObjectCuenta(cuenta) {
        return JSON.stringify({
            _id: cuenta._id,
            account_id: cuenta.account_number_format,
            currency_id: cuenta.currency_id
        });
    }

    _mostrarErrorFrontal(mensaje) {
        this.loading = false;
        this.huboError = true;
        this.mensajeError = mensaje;
    }

    _contruirUrl() {
        return `${MyAppGlobals.rootApiBack}/accounts/${this.numeroCuentaDestino}`;
    }

    _fireEvent(eventName, payload = null) {
        this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, composed: true, detail: payload }));
    }

    continuarTransferencia() {
        console.log("Verificando campos ingresados");

        this.loading = true;

        if (isNaN(this.montoOperacion)) {
            //Se lanza un error por que las claves no coinciden
            console.log('El monto de operación debe ser un número');
            this._mostrarErrorFrontal('El monto de operación debe ser un número');
            return;
        }

        if (this.numeroCuentaDestino.length != 18 && !isNaN(this.numeroCuentaDestino)) {
            //Se lanza un error por que las claves no coinciden
            console.log('El número de cuenta debe ser un número de 18 digitos');
            this._mostrarErrorFrontal('El número de cuenta debe ser un número de 18 digitos');
            return;
        }

        console.log('Lanzando la petición que verifique el número de cuenta destino');

        this.$.doConsultarCuentaTercero.headers['tsec'] = this.tsec;
        this.$.doConsultarCuentaTercero.headers['customerId'] = this.customerId;
        this.$.doConsultarCuentaTercero.url = this._contruirUrl();

        this.$.doConsultarCuentaTercero.generateRequest();

    }

    manejarError(e){
        this.loading = false;
        window.location.href = '/inicio'
    }

    procesarConsultaCuentaTercero(e) {
        window.sessionStorage.setItem('tsec',e.detail.response.tsec);
        let objectTransaccion={}; 

        objectTransaccion.objectCuentaOrigen = JSON.parse(this.objectCuentaOrigen);
        objectTransaccion.montoOperacion = this.montoOperacion;
        objectTransaccion.conceptoOperacion = this.conceptoOperacion;

        objectTransaccion.objectCuentaDestino = {
            _id: e.detail.response.data._id,
            account_id: e.detail.response.data.account_id,
            currency_id: e.detail.response.data.currency_id,
            beneficiario: `${e.detail.response.data.customer.first_name} ${e.detail.response.data.customer.last_name}`
        }
        this._fireEvent('continuar-transferencia-bancaria', objectTransaccion);

        this.huboError = false;
        this.mensajeError = ''
        this.loading = false;
    }

    static get template() {
        return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <pages-util-loading hidden$="[[ !loading ]]"></pages-util-loading>

    <div hidden$="[[ loading ]]"> 
            <div class="container-fluid row mx-0 mt-3" >
              <div class="card col-10 col-sm-10 offset-1" alt="Responsive card">
                <div class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelectCuentaOrigen">Cuenta Origen</label>
                        </div>
                        <select class="custom-select" id="inputGroupSelectCuentaOrigen" value="{{objectCuentaOrigen::change}}">
                            <template is="dom-repeat" items="{{listadoCuentasUsuario}}" as="cuenta" >
                                <option value="[[ _construirObjectCuenta(cuenta) ]]">{{ _resumenCuenta(cuenta) }}</option>
                            </template>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" id="numeroCuentaDestino" placeholder="Número de cuenta destino (Sin guiones)" value="{{numeroCuentaDestino::input}}">
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" id="montoOperacion" placeholder="Monto" value="{{montoOperacion::input}}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="conceptoOperacion" placeholder="Concepto" value="{{conceptoOperacion::input}}" required>
                    </div>
                    <div class="container-fluid row p-0 m-0">
                        <button class="btn btn-primary col-sm-4 offset-4" on-click="continuarTransferencia">Continuar</button>
                    </div>
                    <p 
                    hidden$="[[ !huboError ]]" 
                    class="col-12 col-sm-12 text-center badge-danger mt-2"> 
                        [[ mensajeError ]]
                    </p>
                </div>
              </div>
            </div>
    </div>

    <iron-ajax 
    id="doConsultarCuentaTercero"
    content-type="application/json"
    method="GET"
    handle-as="json"
    on-response="procesarConsultaCuentaTercero"
    on-error="manejarError">
    `;
    }
}

window.customElements.define('pages-operaciones-formulario', pagesOperacionesFormulario);
