/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '../../shared-styles.js';
import '../util/pages-util-loading.js';
import '../util/pages-util-navbar.js';
import './pages-operaciones-formulario.js';
import './pages-operaciones-confirmacion.js';
import './pages-operaciones-constancia';
import '@polymer/iron-ajax';

const OPERACION_STATUS = {
    INITIALIZE: 'initialize',
    CONFIRMATION: 'confirmation',
    PROCCESING: 'proccesing',
    ERROR: 'error',
    DONE: 'done'
};

class PagesOperaciones extends PolymerElement {

    static get properties() {
        return {
            tsec: {
                type: String,
                value: window.sessionStorage.getItem("tsec")
            },
            step: {
                type: String,
                value: OPERACION_STATUS.INITIALIZE
            },
            huboError: {
                type: Boolean,
                value: false
            },
            mensajeError: {
                type: String,
                value: ''
            },
            loading: {
                type: Boolean,
                value: false
            },
            numeroCuentaOrigen: {
                type: Boolean,
                value: ''
            },
            numeroCuentaOrigen: {
                type: Boolean,
                value: ''
            },
            listadoCuentasUsuario: {
                type: Object,
                value: JSON.parse(window.sessionStorage.getItem("listadoCuentas"))
            },
            objectTransferencia: {
                type: Object,
                value: {}
            },
            objectTransaccionCompleta: {
                type: Object,
                value: {}
            }
        };
    }

    connectedCallback(e) {
        console.log(this.listadoCuentasUsuario);
        super.connectedCallback();
        console.log("Verificar presencia de TSEC");
        if (!this.tsec) {
            window.location.href = "/inicio";
            return;
        }
    }

    irInicio(e) {
        window.location.href = '/inicio'
    }

    procesarRespuestaTranferencia(e) {
        this.objectTransaccionCompleta = e.detail.response.data;
        window.sessionStorage.setItem('tsec', e.detail.response.tsec);

        console.log(this.objectTransaccionCompleta);

        this.step = OPERACION_STATUS.DONE;
        this.loading = false;

    }

    manejarError(e){
        alert("Ocurrio un error");
        window.location.href='/inicio';
    }

    _verificarPaso(step, phase) {
        return step === phase;
    }

    _onContinuarTransferenciaBancaria(e) {
        console.log("Realizando la confirmación de la transferencia bancaria");
        console.log(e.detail);
        this.objectTransferencia = e.detail;
        this.step = OPERACION_STATUS.CONFIRMATION;
    }

    _onRealizarTransferenciaBancaria(e) {
        console.log("Realizar la transferencia bancaria");
        this.loading = true;

        this.tsec = window.sessionStorage.getItem('tsec');

        console.log('Lanzando la petición de modificación de clave');
        let requestData = {
            "operation_amount": this.objectTransferencia.montoOperacion,
            "operation_concept_desc": this.objectTransferencia.conceptoOperacion
        };

        this.$.doRealizarTransferencia.headers['tsec'] = window.sessionStorage.getItem('tsec');
        this.$.doRealizarTransferencia.headers['customerId'] = window.sessionStorage.getItem('customerId');
        this.$.doRealizarTransferencia.body = JSON.stringify(requestData);
        this.$.doRealizarTransferencia.url = this._construirUrl();
        this.$.doRealizarTransferencia.generateRequest();

        this.step = OPERACION_STATUS.PROCCESING;
    }

    _onRegresarFormularioTransferenciaBanncaria(e) {
        console.log("Regresando al formulario de transferencia bancaria");
        this.step = OPERACION_STATUS.INITIALIZE;
    }

    _construirUrl() {
        console.log(`${MyAppGlobals.rootApiBack}/accounts/${this.objectTransferencia.objectCuentaOrigen._id}/execute-wire-transfer/${this.objectTransferencia.objectCuentaDestino._id}`);
        return `${MyAppGlobals.rootApiBack}/accounts/${this.objectTransferencia.objectCuentaOrigen._id}/execute-wire-transfer/${this.objectTransferencia.objectCuentaDestino._id}`;
    }

    _onSalirConstancia() {
        this.step = OPERACION_STATUS.INITIALIZE;
        this.objectTransaccionCompleta = {};
        this.objectTransaccion = {};
    }

    static get template() {
        return html`
    <style>
      :host {
        display: block;
        font-family: Helvetica;
      }
    </style>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <pages-util-navbar
        disabled-cuentas = 'false'
        disabled-operaciones = 'true'
    ></pages-util-navbar>

    <pages-util-loading hidden$="[[ !loading ]]"></pages-util-loading>

    <div hidden$="[[ loading ]]"> 
        <div class="container-fluid row text-left m-0 p-0">
            <div class="col-8 col-sm-8 offset-2">
                <h1>OPERACIONES</h1>
                <p>Aquí puedes realizar una transferencia a otra cuenta</p>
                <template is="dom-if" if="[[ _verificarPaso(step, 'initialize') ]]">
                    <pages-operaciones-formulario
                        on-continuar-transferencia-bancaria= "_onContinuarTransferenciaBancaria"
                    >
                    </pages-operaciones-formulario>
                </template>
                <template is="dom-if" if="[[ _verificarPaso(step, 'confirmation') ]]">
                    <pages-operaciones-confirmacion
                        object-transferencia= "[[ objectTransferencia ]]"
                        on-realizar-transferencia-bancaria= "_onRealizarTransferenciaBancaria"
                        on-regresar-formulario-transferencia-bancaria= "_onRegresarFormularioTransferenciaBanncaria">
                    </pages-operaciones-confirmacion>
                </template>
                <template is="dom-if" if="[[ _verificarPaso(step, 'done') ]]">
                    <pages-operaciones-constancia
                        object-transferencia= "[[ objectTransferencia ]]"
                        object-transferencia-completa= "[[ objectTransaccionCompleta ]]"
                        on-salir-constancia= "_onSalirConstancia"
                    </pages-operaciones-constancia>
                </template>
            </div>
        </div>
    </div>

    <iron-ajax 
    id="doRealizarTransferencia"
    content-type="application/json"
    method="POST"
    handle-as="json"
    on-response="procesarRespuestaTranferencia"
    on-error="manejarError">

    `;
    }
}

window.customElements.define('pages-operaciones', PagesOperaciones);
